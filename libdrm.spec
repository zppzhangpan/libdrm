%define bcond_meson() %{lua: do
  local option = rpm.expand("%{1}")
  local with = rpm.expand("%{?with_" .. option .. "}")
  local value = (with ~= '') and "true" or "false"
  option = option:gsub('_', '-')
  print(string.format("-D%s=%s", option, value))
end}

%bcond_without libkms
%ifarch %{ix86} x86_64
%bcond_without intel
%else
%bcond_with    intel
%endif
%bcond_without radeon
%bcond_without amdgpu
%bcond_without nouveau
%bcond_without vmwgfx
%ifarch %{arm}
%bcond_without omap
%else
%bcond_with    omap
%endif
%ifarch %{arm} aarch64
%bcond_without exynos
%bcond_without freedreno
%bcond_without tegra
%bcond_without vc4
%bcond_without etnaviv
%else
%bcond_with    exynos
%bcond_with    freedreno
%bcond_with    tegra
%bcond_with    vc4
%bcond_with    etnaviv
%endif
%bcond_with    cairo_tests
%bcond_without man_pages
%ifarch %{valgrind_arches}
%bcond_without valgrind
%else
%bcond_with    valgrind
%endif
%bcond_with    freedreno_kgsl
%bcond_without install_test_programs
%bcond_without udev

Name:           libdrm
Version:        2.4.109
Release:        7
License:        MIT
Summary:        library for direct rendering manager

URL:            https://dri.freedesktop.org
Source0:        https://dri.freedesktop.org/libdrm/%{name}-%{version}.tar.xz
Source1:	https://dri.freedesktop.org/libdrm/%{name}-%{version}.tar.xz.sig
Source2:        91-drm-modeset.rules

BuildRequires:  meson >= 0.43  gcc kernel-headers
%if %{with intel}
BuildRequires:  pkgconfig(pciaccess) >= 0.10
%endif

%if %{with cairo_tests}
BuildRequires:  pkgconfig(cairo)
%endif
%if %{with man_pages}
BuildRequires:  %{_bindir}/xsltproc
BuildRequires:  %{_bindir}/sed
BuildRequires:  python3-docutils chrpath
%endif
%if %{?openEuler:1}0
%if %{with valgrind}
BuildRequires:  valgrind-devel
%endif
%endif
%if %{with udev}
BuildRequires:  pkgconfig(udev)
%endif

Patch1001:      %{name}-make-dri-perms-okay.patch
Patch1002:      %{name}-2.4.0-no-bc.patch
%ifarch sw_64
Patch1003:      libdrm-2.4.109-sw.patch
%endif

%description
%{name} provides a user space library for accessing the DRM, direct rendering manager, 
on operating systems that support the ioctl interface. libdrm is a low-level library, 
typically used by graphics drivers such as the Mesa DRI drivers, the X drivers, libva 
and similar projects.

%package 	devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       kernel-headers

%description 	devel
%{name}-devel contains the header files for developing
applications that want to make use of %{name}.

%if %{with install_test_programs}
%package 	-n drm-utils
Summary:        Utilities programs for %{name}
Requires:       %{name} = %{version}-%{release}

%description 	-n drm-utils
Utility programs for the kernel DRM interface. 
%endif

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson \
  %{bcond_meson libkms} %{bcond_meson intel} %{bcond_meson radeon} %{bcond_meson amdgpu}                \
  %{bcond_meson nouveau} %{bcond_meson vmwgfx} %{bcond_meson omap} %{bcond_meson exynos}                \
  %{bcond_meson freedreno} %{bcond_meson tegra} %{bcond_meson vc4} %{bcond_meson etnaviv}               \
  %{bcond_meson cairo_tests} %{bcond_meson man_pages} \
%if %{?openEuler:1}0
  %{bcond_meson valgrind} \
%endif
  %{bcond_meson freedreno_kgsl} %{bcond_meson install_test_programs} %{bcond_meson udev}                  \
  %{nil}
%meson_build

%check
%meson_test

%install
%meson_install
%if %{with install_test_programs}
install -Dpm0755 -t %{buildroot}%{_bindir} %{_vpath_builddir}/tests/drmdevice
%endif
%if %{with udev}
install -Dpm0644 -t %{buildroot}%{_udevrulesdir} %{S:2}
%endif

%ldconfig_scriptlets

chrpath --delete $RPM_BUILD_ROOT%{_bindir}/drmdevice

%files
%defattr(-,root,root)
%{_libdir}/%{name}.so.*
%dir %{_datadir}/%{name}/
%if %{with libkms}
%{_libdir}/libkms.so.*
%endif
%if %{with intel}
%{_libdir}/%{name}_intel.so.*
%endif
%if %{with radeon}
%{_libdir}/%{name}_radeon.so.*
%endif
%if %{with amdgpu}
%{_libdir}/%{name}_amdgpu.so.*
%{_datadir}/%{name}/amdgpu.ids
%endif
%if %{with nouveau}
%{_libdir}/%{name}_nouveau.so.*
%endif
%if %{with omap}
%{_libdir}/%{name}_omap.so.*
%endif
%if %{with exynos}
%{_libdir}/%{name}_exynos.so.*
%endif
%if %{with freedreno}
%{_libdir}/%{name}_freedreno.so.*
%endif
%if %{with tegra}
%{_libdir}/%{name}_tegra.so.*
%endif
%if %{with etnaviv}
%{_libdir}/%{name}_etnaviv.so.*
%endif
%if %{with udev}
%{_udevrulesdir}/91-drm-modeset.rules
%endif

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}/drm*.h
%{_includedir}/%{name}/*_drm.h
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}.pc
%if %{with libkms}
%{_includedir}/libkms/
%{_libdir}/libkms.so
%{_libdir}/pkgconfig/libkms.pc
%endif
%if %{with intel}
%{_includedir}/%{name}/intel_*.h
%{_libdir}/%{name}_intel.so
%{_libdir}/pkgconfig/%{name}_intel.pc
%endif
%if %{with radeon}
%{_includedir}/%{name}/radeon_*.h
%{_includedir}/%{name}/r600_pci_ids.h
%{_libdir}/%{name}_radeon.so
%{_libdir}/pkgconfig/%{name}_radeon.pc
%endif
%if %{with amdgpu}
%{_includedir}/%{name}/amdgpu.h
%{_libdir}/%{name}_amdgpu.so
%{_libdir}/pkgconfig/%{name}_amdgpu.pc
%endif
%if %{with nouveau}
%{_includedir}/%{name}/nouveau/
%{_libdir}/%{name}_nouveau.so
%{_libdir}/pkgconfig/%{name}_nouveau.pc
%endif
%if %{with omap}
%{_includedir}/%{name}/omap_*.h
%{_includedir}/omap/
%{_libdir}/%{name}_omap.so
%{_libdir}/pkgconfig/%{name}_omap.pc
%endif
%if %{with exynos}
%{_includedir}/%{name}/exynos_*.h
%{_includedir}/exynos/
%{_libdir}/%{name}_exynos.so
%{_libdir}/pkgconfig/%{name}_exynos.pc
%endif
%if %{with freedreno}
%{_includedir}/freedreno/
%{_libdir}/%{name}_freedreno.so
%{_libdir}/pkgconfig/%{name}_freedreno.pc
%endif
%if %{with tegra}
%{_includedir}/%{name}/tegra.h
%{_libdir}/%{name}_tegra.so
%{_libdir}/pkgconfig/%{name}_tegra.pc
%endif
%if %{with vc4}
%{_includedir}/%{name}/vc4_*.h
%{_libdir}/pkgconfig/%{name}_vc4.pc
%endif
%if %{with etnaviv}
%{_includedir}/%{name}/etnaviv_*.h
%{_libdir}/%{name}_etnaviv.so
%{_libdir}/pkgconfig/%{name}_etnaviv.pc
%endif
%{_includedir}/*.h

%if %{with install_test_programs}
%files -n drm-utils
%defattr(-,root,root)
%{_bindir}/amdgpu_stress
%{_bindir}/drmdevice
%exclude %{_bindir}/etnaviv_*
%exclude %{_bindir}/exynos_*
%{_bindir}/kms-steal-crtc
%{_bindir}/kms-universal-planes
%if %{with libkms}
%{_bindir}/kmstest
%endif
%{_bindir}/modeprint
%{_bindir}/modetest
%{_bindir}/proptest
%{_bindir}/vbltest
%endif

%files help
%defattr(-,root,root)
%if %{with man_pages}
%{_mandir}/man3/drm*.3*
%{_mandir}/man7/drm*.7*
%endif

%changelog
* Thu Dec 08 2022 zhouwenpei<zhouwenpei1@h-partners.com> - 2.4.109-7
- remove rpath

* Fri Nov 18 2022 zhouwenpei<zhouwenpei1@h-partners.com> - 2.4.109-6
- Optimize the patch for add sw architecture

* Fri Nov 18 2022 wuzx<wuzx1226@qq.com> - 2.4.109-5
- Add sw64 architecture

* Tue Oct 18 2022 Liu Zixian <liuzixian4@huawei.com> - 2.4.109-4
- remove unused BuildRequires

* Wed Mar 30 2022 liuyumeng <liuyumeng5@h-partners.com> - 2.4.109-3
- enable tests

* Tue Mar 1 2022 hanhui <hanhui15@h-partners.com> - 2.4.109-2
* customized valgrind

* Fri Dec 3 2021 xingxing <xingxing9@huawei.com> - 2.4.109-1
- update to 2.4.109

* Fri Jan 29 2021 zhanzhimin <zhanzhimin@huawei.com> - 2.4.103-1
- update to 2.4.103

* Mon Jul 27 2020 chengguipeng<chengguipeng1@huawei.com> - 2.4.102-1
- Type:bugfix
- Id:NA
- SUG:NA
- update to 2.4.102


* Wed Apr 15 2020 songnannan <songnannan2@huawei.com> - 2.4.100-1
- Type:bugfix
- Id:NA
- SUG:NA
- update to 2.4.100

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.4.95-2
- Package init
